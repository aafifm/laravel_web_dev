@extends('layout.master')

@section('title')
Edit Cast
@endsection

@section('judul')
Edit Cast
@endsection

@section('content')
<h1>Formulir Edit Pemain</h1>
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="string" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message}}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea type="text" name="bio" class="form-control">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror   
    <button type="submit" class="bten btn-primary">Submit</button>
</form>

@endsection