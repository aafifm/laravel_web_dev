@extends('layout.master')

@section('title')
Data Cast Detail
@endsection


@section('content')

<h1>{{$cast->nama}}</h1>
<p>Berusia {{$cast->umur}} tahun</p>
<p>{{$cast->bio}}</p>

<a href="/cast/" class="btn btn-info btn-sm">Back</a>

@endsection