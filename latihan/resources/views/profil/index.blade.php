@extends('layout.master')

@section('judul')
Halama Update Profil
@endsection

@section('content')

<form action="/profil/{{$profil->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Nama User</label>
        <input type="text"  value="{{$profil->user->name}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Email User</label>
        <input type="text" value="{{$profil->user->email}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Umur</label>
        <input type="integer" name="umur" value="{{$profil->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror

    <div class="form-group">
        <label>Biodata</label>
        <textarea type="text" name="bio" class="form-control">{{$profil->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror   

    <div class="form-group">
        <label>Alamat</label>
        <textarea type="text" name="alamat" class="form-control">{{$profil->alamat}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror

    <button type="submit" class="bten btn-primary">Submit</button>
</form>

@endsection