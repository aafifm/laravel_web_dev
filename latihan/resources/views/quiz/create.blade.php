<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Create Data</title>

</head>

<body>

<h2>Create Data Game</h2>

//Code disini
// name //

<div class="form-group">

    <label>Nama Game</label>

    <input type="string" name="name" class="form-control">

</div>

@error('nama')

    <div class="alert alert-danger">{{ $message}}</div>

@enderror

// gameplay //

<div class="form-group">

    <label>Gameplay</label>

    <textarea type="text" name="gameplay" class="form-control"></textarea>

</div>

@error('gameplay')

<div class="alert alert-danger">{{ $message}}</div>

@enderror  

// developer //

<div class="form-group">

    <label>Nama Developer</label>

    <input type="string" name="developer" class="form-control">

</div>

@error('developer')

    <div class="alert alert-danger">{{ $message}}</div>

@enderror

// year //

<div class="form-group">

    <label>Released Year</label>

    <input type="string" name="year" class="form-control">

</div>

@error('year')

    <div class="alert alert-danger">{{ $message}}</div>

@enderror

<button type="submit" class="btn btn-primary">submit</button>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>