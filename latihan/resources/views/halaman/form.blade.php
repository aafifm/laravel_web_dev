@extends('layout.master')

@section('title')
Formulir Pendaftaran
@endsection

@section('judul')
Formulir Pendaftaran
@endsection

@section('content')
<h1>Buat Account Baru</h1>
<form action="/welcome" method="post">
    @csrf
    <label>First Name</label><br>
    <input type="text" name="nama_depan"><br><br>

    <label>Last Name</label><br>
    <input type="text" name="nama_belakang"><br><br>
    
    <label>Gender</label><br>
    <input type="radio" name="sex">Male<br>
    <input type="radio" name="sex">Female<br><br>

    <label>Nationality</label><br>
    <select name="nationality" id="">
        <option value="1">Indonesia</option>
        <option value="2">Amerika</option>
        <option value="3">Inggris</option>
    </select><br><br>

    <label>Language Spoken</label>
    <br>
    <input type="checkbox" name="language">Bahasa Indonesia 
    <br>
    <input type="checkbox" name="language">English 
    <br>
    <input type="checkbox" name="language">Other 
    <br><br>

    <label>Bio</label> <br>
    <textarea name="bio" cols="30" rows="10"></textarea> <br><br>

    <input type="submit" value="submit">
</form>

@endsection