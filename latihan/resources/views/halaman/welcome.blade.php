@extends('layout.master')

@section('title')
Welcome Page
@endsection

@section('judul')
Welcome Page
@endsection

@section('content')
    <h1>Selamat Datang {{$nama_depan}} {{$nama_belakang}} ! </h1>
    <p><b>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</b></p>
@endsection

