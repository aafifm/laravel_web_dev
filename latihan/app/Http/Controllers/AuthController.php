<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.form');
    }

    public function submit(Request $request){
        $nama_depan = $request['nama_depan'];
        $nama_belakang = $request['nama_belakang'];
        return view('halaman.welcome', compact('nama_depan','nama_belakang'));
    }
}
