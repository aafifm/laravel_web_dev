<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profil;

class ProfilController extends Controller
{
    public function index(){
        $profil = Profil::where('user_id', Auth::id())->first();

        return view('profil.index', compact('profil'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        $profil = Profil::find($id);

        $profil->umur = $request['umur'];
        $profil->bio = $request['bio'];
        $profil->alamat = $request['alamat'];

        $profil->save();

        return redirect('/profil');
    }
}
