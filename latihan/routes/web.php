<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@submit');

Route::get('/data-tables', 'IndexController@dataTable');

Route::get('/master', function(){
    return view('layout.master');
});

Route::group(['middleware' => ['auth']], function () {
    // CRUD Cast

    //menampilkan list data para pemain film 
    Route::get('/cast','CastController@index'); 

    //menampilkan form untuk membuat data pemain film baru
    Route::get('/cast/create','CastController@create'); 

    //menyimpan data baru ke tabel Cast
    Route::post('/cast','CastController@store'); 

    //menampilkan detail data pemain film dengan id tertentu
    Route::get('/cast/{cast_id}','CastController@show'); 

    //menampilkan form untuk edit pemain film dengan id tertentu
    Route::get('/cast/{cast_id}/edit','CastController@edit'); 

    //menyimpan perubahan data pemain film (update) untuk id tertentu
    Route::put('/cast/{cast_id}','CastController@update'); 

    //	menghapus data pemain film dengan id tertentu
    Route::delete('/cast/{cast_id}','CastController@destroy'); 


    // Update Profil
    Route::resource('profil', 'ProfilController')->only([
        'index','update'
    ]);

    //menampilkan form untuk membuat data  film baru
    Route::get('/film/create','FilmController@create'); 

    //menyimpan data baru ke laman film
    Route::post('/film','FilmController@store'); 



    //menampilkan form untuk edit  film dengan id tertentu
    Route::get('/film/{film_id}/edit','FilmController@edit'); 

    //menyimpan perubahan data  film (update) untuk id tertentu
    Route::put('/film/{film_id}','FilmController@update'); 

    //	menghapus data film dengan id tertentu
    Route::delete('/film/{film_id}','FilmController@destroy'); 

});

//menampilkan list film  
 Route::get('/film','FilmController@index'); 

 //menampilkan detail data  film dengan id tertentu
Route::get('/film/{film_id}','FilmController@show'); 

// Update Profil
Route::resource('profil', 'ProfilController')->only([
        'index','update']); 

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();